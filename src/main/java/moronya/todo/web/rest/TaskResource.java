package moronya.todo.web.rest;

import moronya.todo.domain.Task;
import moronya.todo.domain.User;
import moronya.todo.service.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class TaskResource {
    private final TaskService taskService;
    private final Logger logger = LoggerFactory.getLogger(TaskResource.class);
    Task task = new Task();

    public TaskResource(TaskService taskService) {
        this.taskService = taskService;
    }
    @PostMapping("/tasks")
    public Task save(@RequestBody Task task){
        logger.debug("Request to save task");

        return taskService.saveTask(task);
    }
    @DeleteMapping("tasks/{id}")
    public void delete(@PathVariable Long id){
        logger.debug("Request to delete task");
        taskService.deleteById(id);


    }

}
