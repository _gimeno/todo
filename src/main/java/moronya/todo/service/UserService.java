package moronya.todo.service;

import moronya.todo.UserRepository.UserRepository;
import moronya.todo.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.mail.MessagingException;

@Service
public class UserService {
 private  final UserRepository userRepository;
 private final Logger logger = LoggerFactory.getLogger(UserService.class);
 private final MailService mailService;
 private final PasswordEncoder passwordEncoder;
 User user = new User();


    public UserService(UserRepository userRepository, MailService mailService, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;

        this.mailService = mailService;
        this.passwordEncoder = passwordEncoder;
    }
    //save user
    public User saveUser(User user) throws MessagingException {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        logger.debug("Request to save user with: {}", user);
        userRepository.save(user);
        mailService.welcomeMail(user);
        return user;
    }
}
