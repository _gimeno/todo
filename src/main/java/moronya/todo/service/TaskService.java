package moronya.todo.service;

import moronya.todo.UserRepository.TaskRepository;
import moronya.todo.domain.Status;
import moronya.todo.domain.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class TaskService {
    private final TaskRepository taskRepository;
    private final Logger logger = LoggerFactory.getLogger(TaskService.class);
    Task task = new Task();

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }
    //save task
    public Task saveTask(Task task){
        task.setStatus(Status.DO);
        logger.debug("Request to save task: {}", task);
        return taskRepository.save(task);

    }
    //delete task
    public void deleteById(Long id){
        logger.debug("Request to delete task with id: {}", id);
        taskRepository.deleteById(id);
    }
}
