package moronya.todo.service;

import moronya.todo.config.ApplicationProperties;
import moronya.todo.domain.User;
import moronya.todo.service.utility.AuthTemplateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class MailService {
    private final Logger logger = LoggerFactory.getLogger(MailService.class);

    private final JavaMailSender javaMailSender;

    private final ApplicationProperties applicationProperties;

    public MailService(JavaMailSender javaMailSender, ApplicationProperties applicationProperties) {
        this.javaMailSender = javaMailSender;
        this.applicationProperties = applicationProperties;
    }
    @Async
    public void welcomeMail(User user) throws MessagingException{
        String name = user.getUsername();
        String phoneNumber = applicationProperties.getPhoneNumber();
        String companyName = applicationProperties.getCompanyName();
        String content = AuthTemplateUtil.getAccountCreationEmail(name, companyName, phoneNumber);
        String subject = applicationProperties.getCompanyName() + " Account creation";
        sendEmail(user.getEmail(),subject,content,false,true);
    }
    @Async
    public void sendEmail(String to, String subject, String body, boolean isMultipart, boolean isHtml) throws MessagingException{
        logger.info("Request to send mail to {} with body {}", to, body);
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, isMultipart);
        mimeMessageHelper.setSubject(subject);
        mimeMessageHelper.setTo(to);
        mimeMessageHelper.setText(body, isHtml);
        javaMailSender.send(mimeMessage);
    }
}
